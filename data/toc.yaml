- name: Company
  icon: fas fa-brands fa-gitlab
  url: https://about.gitlab.com
  links:
    - name: About GitLab
      url: https://about.gitlab.com/company/
      links:
      - name: History
        url: https://about.gitlab.com/company/history/
    - name: Values
      url: /handbook/values/
    - name: Mission
      url: https://about.gitlab.com/company/mission/
    - name: Vision
      url: https://about.gitlab.com/company/vision/
    - name: Strategy
      url: https://about.gitlab.com/company/strategy/
    - name: Communication
      url: /handbook/communication/
      links:
        - name: YouTube
          url: https://about.gitlab.com/handbook/marketing/marketing-operations/youtube/
        - name: Zoom Webinars
          url: https://about.gitlab.com/handbook/communication/zoom/webinars/
    - name: Culture
      url: https://about.gitlab.com/company/culture/
      links:
        - name: All-Remote
          url: https://about.gitlab.com/company/culture/all-remote/guide/
        - name: Life at GitLab
          url: https://about.gitlab.com/company/culture/#life-at-gitlab
        - name: GitLab Contribute
          url: https://about.gitlab.com/events/gitlab-contribute/
        - name: Team
          url: https://about.gitlab.com/company/team/
        - name: Only healthy constraints
          url: https://about.gitlab.com/handbook/only-healthy-constraints
    - name: TeamOps
      url: https://about.gitlab.com/teamops/
    - name: CEO Readme
      url: https://about.gitlab.com/handbook/ceo/
      links:
        - name: CEO Shadow Program
          url: https://about.gitlab.com/handbook/ceo/shadow/
        - name: Cadence
          url: https://about.gitlab.com/company/cadence/
        - name: E-Group Offsite
          url: https://about.gitlab.com/company/offsite/
        - name: KPIs
          url: https://about.gitlab.com/company/kpis/
        - name: OKRs
          url: https://about.gitlab.com/company/okrs/
        - name: Pricing Model
          url: https://about.gitlab.com/company/pricing/
    - name: Creator pairing
      url: https://about.gitlab.com/handbook/engineering/fellow/shadow/
    - name: Chief of Staff Team to the CEO
      url: https://about.gitlab.com/handbook/ceo/chief-of-staff-team/
    - name: Key Reviews
      url: https://about.gitlab.com/handbook/key-review/
    - name: Group Conversations
      url: https://about.gitlab.com/handbook/group-conversations/
    - name: E-Group Weekly
      url: https://about.gitlab.com/handbook/e-group-weekly/
    - name: Sustainability
      url: https://about.gitlab.com/handbook/environmental-sustainability/
- name: Handbook
  icon: fa-solid fa-book
  url: /handbook/about
  links:
  - name: Handbook Preferences
    url: /preferences
  - name: Handbook Changelog
    url: /handbook/about/changelog
  - name: Handbook Roadmap
    url: /handbook/about/roadmap
  - name: Handbook Support
    url: /handbook/about/support
  - name: Handbook On-Call
    url: /handbook/about/on-call
  - name: Handbook Usage
    url: https://about.gitlab.com/handbook/handbook-usage/
  - name: Contribution Guide
    url: /docs
  - name: Practical Handbook Edits
    url: https://about.gitlab.com/handbook/practical-handbook-edits/
  - name: Edit This Website Locally
    url: https://about.gitlab.com/handbook/git-page-update/
  - name: Handbook Style Guide
    url: https://about.gitlab.com/handbook/style-guide/
  - name: Content Websites Responsibility
    url: /handbook/content-websites
- name: People Group
  icon: fa-solid fa-people-group
  url: https://about.gitlab.com/handbook/people-group/
  links:
    - name: Anti-Harassment Policy
      url: https://about.gitlab.com/handbook/anti-harassment/
    - name: Global Volunteer Month
      url: https://about.gitlab.com/handbook/people-group/givelab-volunteer-initiatives/
    - name: Hiring
      url: https://about.gitlab.com/handbook/hiring/
      links:
        - name: Greenhouse
          url: https://about.gitlab.com/handbook/hiring/greenhouse/
        - name: Interviewing
          url: https://about.gitlab.com/handbook/hiring/interviewing/
        - name: Jobs FAQ
          url: https://about.gitlab.com/handbook/hiring/candidate/faq/
        - name: Vacancies
          url: https://about.gitlab.com/handbook/hiring/vacancies/
    - name: Inclusion & Diversity
      url: https://about.gitlab.com/company/culture/inclusion/
      links:
        - name: Ally Resources
          url: https://about.gitlab.com/handbook/communication/ally-resources/
        - name: Gender and Sexual Orientation Identity Definitions and FAQ
          url: https://about.gitlab.com/handbook/people-group/orientation-identity/
        - name: Unconscious Bias
          url: https://about.gitlab.com/company/culture/inclusion/unconscious-bias/
    - name: Labor and Employment Notices
      url: https://about.gitlab.com/handbook/labor-and-employment-notices/
    - name: Leadership
      url: https://about.gitlab.com/handbook/leadership/
    - name: Learning & Development
      url: https://about.gitlab.com/handbook/people-group/learning-and-development/
    - name: Onboarding
      url: https://about.gitlab.com/handbook/people-group/general-onboarding/
    - name: Offboarding
      url: https://about.gitlab.com/handbook/people-group/offboarding/
    - name: Spending Company Money
      url: https://about.gitlab.com/handbook/spending-company-money/
      links:
        - name: Travel
          url: https://about.gitlab.com/handbook/travel/
        - name: Visas
          url: https://about.gitlab.com/handbook/people-group/visas/
    - name: Team Member Relations Philosophy
      url: https://about.gitlab.com/handbook/people-group/team-member-relations/#team-member-relations-philosophy
    - name: Total Rewards
      url: https://about.gitlab.com/handbook/total-rewards/
      links:
        - name: Benefits
          url: https://about.gitlab.com/handbook/total-rewards/benefits/
        - name: Incentives
          url: https://about.gitlab.com/handbook/incentives/
        - name: Paid time off
          url: https://about.gitlab.com/handbook/paid-time-off/
        - name: Compensation
          url: https://about.gitlab.com/handbook/total-rewards/compensation/
        - name: Stock Options
          url: https://about.gitlab.com/handbook/stock-options/
    - name: Tools and Tips
      url: https://about.gitlab.com/handbook/tools-and-tips/
      links:
        - name: Searching the GitLab Website Like a Pro
          url: https://about.gitlab.com/handbook/tools-and-tips/searching/
- name: Engineering
  icon: fa-solid fa-laptop-code
  url: https://about.gitlab.com/handbook/engineering/
  links:
  - name: Customer Support Department
    url: /handbook/support/
  - name: Development Department
    url: https://about.gitlab.com/handbook/engineering/development/
    links:
    - name: Dev Sub-department
      url: https://about.gitlab.com/handbook/engineering/development/dev/
    - name: Enablement Sub-department
      url: https://about.gitlab.com/handbook/engineering/development/enablement/
    - name: Fulfillment Sub-department
      url: https://about.gitlab.com/handbook/engineering/development/fulfillment/
    - name: Growth Sub-department
      url: https://about.gitlab.com/handbook/engineering/development/growth/
    - name: Ops Sub-department
      url: https://about.gitlab.com/handbook/engineering/development/ops/
    - name: Govern Sub-department
      url: https://about.gitlab.com/handbook/engineering/development/sec/govern/
    - name: Secure Sub-department
      url: https://about.gitlab.com/handbook/engineering/development/sec/secure/
  - name: Incubation Engineering Department
    url: https://about.gitlab.com/handbook/engineering/incubation/
  - name: Infrastructure Department
    url: https://about.gitlab.com/handbook/engineering/infrastructure/
  - name: Quality Department
    url: https://about.gitlab.com/handbook/engineering/quality/
    links:
    - name: Quality Engineering
      url: https://about.gitlab.com/handbook/engineering/quality/quality-engineering/
    - name: Engineering Productivity
      url: https://about.gitlab.com/handbook/engineering/quality/engineering-productivity/
    - name: Engineering Analytics
      url: https://about.gitlab.com/handbook/engineering/quality/engineering-analytics/
  - name: Security Practices
    url: https://about.gitlab.com/handbook/security/
  - name: Open Source
    url: https://about.gitlab.com/handbook/engineering/open-source/
- name: Security
  icon: fa-solid fa-shield-halved
  url: https://about.gitlab.com/handbook/security/
  links:
  - name: Security Engineering
    url: https://about.gitlab.com/handbook/security/#secure-the-product--security-engineering
  - name: Security Operations
    url: https://about.gitlab.com/handbook/security/security-operations/
  - name: Threat Management
    url: https://about.gitlab.com/handbook/security/threat-management/
  - name: Security Assurance
    url: https://about.gitlab.com/handbook/security/security-assurance/
- name: Marketing
  icon: fa-solid fa-lightbulb
  url: https://about.gitlab.com/handbook/marketing/
  links:
    - name: Team Member Social Media Policy
      url: https://about.gitlab.com/handbook/marketing/team-member-social-media-policy/
    - name: Blog
      url: https://about.gitlab.com/handbook/marketing/blog/
    - name: Brand and Product Marketing
      url: https://about.gitlab.com/handbook/marketing/brand-and-product-marketing/
      links:
        - name: Brand
          url: https://about.gitlab.com/handbook/marketing/brand-and-product-marketing/brand/
          links:
            - name: Merchandise Handling (Swag)
              url: https://about.gitlab.com/handbook/marketing/brand-and-product-marketing/brand/merchandise-handling/
        - name: Product and Solution Marketing
          url: https://about.gitlab.com/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/
          links:
            - name: Demos
              url: https://about.gitlab.com/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/demo/
            - name: Core Product Marketing
              url: https://about.gitlab.com/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/core-product-marketing/
            - name: Technical Marketing
              url: https://about.gitlab.com/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/technical-marketing/
            - name: Competitive intelligence
              url: https://about.gitlab.com/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/competitive-intelligence/
            - name: Customer Advocacy
              url: https://about.gitlab.com/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/customer-advocacy/
        - name: Design
          url: https://about.gitlab.com/handbook/marketing/brand-and-product-marketing/design/
        - name: Content
          url: https://about.gitlab.com/handbook/marketing/brand-and-product-marketing/content/
    - name: Integrated Marketing
      url: https://about.gitlab.com/handbook/marketing/integrated-marketing/
      links:
        - name: Field Marketing
          url: https://about.gitlab.com/handbook/marketing/field-marketing/
        - name: Channel & Alliances Marketing
          url: https://about.gitlab.com/handbook/marketing/channel-marketing/
        - name: Corporate Events
          url: https://about.gitlab.com/handbook/marketing/integrated-marketing/corporate-events/
        - name: Account Based Marketing
          url: https://about.gitlab.com/handbook/marketing/account-based-marketing/
        - name: Campaigns
          url: https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/
        - name: Lifecycle Marketing
          url: https://about.gitlab.com/handbook/marketing/lifecycle-marketing/
        - name: Digital Strategy
          url: https://about.gitlab.com/handbook/marketing/integrated-marketing/digital-strategy/
        - name: Digital Experience
          url: https://about.gitlab.com/handbook/marketing/digital-experience/
          links:
            - name: Marketing Site
              url: https://about.gitlab.com/handbook/marketing/digital-experience/website/
    - name: Sales Development
      url: https://about.gitlab.com/handbook/marketing/sales-development/
    - name: Marketing Strategy, Ops, & Analytics
      url: https://about.gitlab.com/handbook/marketing/marketing-strategy-and-platforms/
      links:
        - name: Marketing Operations
          url: https://about.gitlab.com/handbook/marketing/marketing-operations/
        - name: Marketing Strategy & Analytics
          url: https://about.gitlab.com/handbook/marketing/strategy-performance/
    - name: Growth
      url: https://about.gitlab.com/handbook/marketing/growth
    - name: Developer Relations & Community
      url: https://about.gitlab.com/handbook/marketing/developer-relations/
      links:
      - name: Evangelist Program
        url: https://about.gitlab.com/handbook/marketing/developer-relations/evangelist-program/
      - name: Community Programs
        url: https://about.gitlab.com/handbook/marketing/developer-relations/community-programs/
      - name: Developer Evangelism
        url: https://about.gitlab.com/handbook/marketing/developer-relations/developer-evangelism/
      - name: Contributor Success
        url: https://about.gitlab.com/handbook/marketing/developer-relations/contributor-success/
    - name: Corporate Communications
      url: https://about.gitlab.com/handbook/marketing/corporate-communications/
      links:
      - name: Analyst Relations
        url: https://about.gitlab.com/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/analyst-relations/
      - name: Speaking Resources
        url: https://about.gitlab.com/handbook/marketing/corporate-communications/speaking-resources/
      - name: Incident Communications Plan
        url: https://about.gitlab.com/handbook/marketing/corporate-communications/incident-communications-plan/
- name: Sales
  icon: fa-solid fa-handshake
  url: https://about.gitlab.com/handbook/sales/
  links:
  - name: Commercial
    url: https://about.gitlab.com/handbook/sales/commercial/
  - name: Customer Success
    url: https://about.gitlab.com/handbook/customer-success/
  - name: Reseller Channels
    url: https://about.gitlab.com/handbook/resellers/
  - name: Field Operations
    url: https://about.gitlab.com/handbook/sales/field-operations/
    links:
    - name: Sales Operations
      url: https://about.gitlab.com/handbook/sales/field-operations/sales-operations/
      links:
      - name: Deal Desk
        url: https://about.gitlab.com/handbook/sales/field-operations/sales-operations/deal-desk/
      - name: Data Intelligence
        url: https://about.gitlab.com/handbook/sales/field-operations/data-intelligence/
    - name: Field Enablement
      url: https://about.gitlab.com/handbook/sales/field-operations/field-enablement/
    - name: Sales Strategy
      url: https://about.gitlab.com/handbook/sales/field-operations/sales-strategy/
    - name: Sales Systems
      url: https://about.gitlab.com/handbook/sales/field-operations/sales-systems/
  - name: Reporting
    url: https://about.gitlab.com/handbook/business-technology/#reporting
  - name: Customer Success Management
    url: https://about.gitlab.com/handbook/customer-success/csm/
  - name: Alliances
    url: https://about.gitlab.com/handbook/alliances/
- name: Finance
  icon: fa-solid fa-landmark
  url: https://about.gitlab.com/handbook/finance/
  links:
  - name: Accounts Payable
    url: https://about.gitlab.com/handbook/finance/accounts-payable/
  - name: Accounts Receivable
    url: https://about.gitlab.com/handbook/finance/accounting/#accounts-receivable/
  - name: Business Technology
    url: https://about.gitlab.com/handbook/business-technology/
  - name: Enterprise Risk Management
    url: https://about.gitlab.com/handbook/finance/enterprise-risk-management/
  - name: Expenses
    url: https://about.gitlab.com/handbook/finance/expenses/
  - name: Financial Planning & Analysis
    url: https://about.gitlab.com/handbook/finance/financial-planning-and-analysis/
  - name: Payroll
    url: https://about.gitlab.com/handbook/finance/payroll/
  - name: Procurement
    url: https://about.gitlab.com/handbook/finance/procurement/
  - name: Tax
    url: https://about.gitlab.com/handbook/tax/
  - name: Board meetings
    url: https://about.gitlab.com/handbook/board-meetings/
  - name: Internal Audit
    url: https://about.gitlab.com/handbook/internal-audit/
    links:
     - name: Sarbanes-Oxley (SOX) Compliance
       url: https://about.gitlab.com/handbook/internal-audit/sarbanes-oxley/
  - name: Stock Options
    url: https://about.gitlab.com/handbook/stock-options/
- name: Product
  icon: fa-solid fa-comments
  url: https://about.gitlab.com/handbook/product/
  links:
  - name: Release posts
    url: https://about.gitlab.com/handbook/marketing/blog/release-posts/
  - name: About the GitLab Product
    url: https://about.gitlab.com/handbook/product/gitlab-the-product/
  - name: Being a Product Manager at GitLab
    url: https://about.gitlab.com/handbook/product/product-manager-role/
  - name: Product Principles
    url: https://about.gitlab.com/handbook/product/product-principles/
  - name: Product Processes
    url: https://about.gitlab.com/handbook/product/product-processes/
  - name: Product sections, stages, groups, and categories
    url: https://about.gitlab.com/handbook/product/categories/
  - name: Product Development Flow
    url: https://about.gitlab.com/handbook/product-development-flow/
  - name: Product Development Timeline
    url: https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline
  - name: Data for Product Managers
    url: https://about.gitlab.com/handbook/business-technology/data-team/programs/data-for-product-managers/
  - name: Product Pricing Model
    url: https://about.gitlab.com/company/pricing/
  - name: Data analysis
    url: https://about.gitlab.com/handbook/business-technology/data-team/
  - name: Corporate Development / Acquisitions
    url: https://about.gitlab.com/handbook/acquisitions/
  - name: UX Department
    url: https://about.gitlab.com/handbook/product/ux/
    links:
    - name: Technical Writing
      url: https://about.gitlab.com/handbook/product/ux/technical-writing/
    - name: UX Research
      url: https://about.gitlab.com/handbook/product/ux/ux-research/
    - name: Product Design
      url: https://about.gitlab.com/handbook/product/ux/product-design/
- name: Legal and Compliance
  icon: fa-solid fa-scale-balanced
  url: https://about.gitlab.com/handbook/legal/
  links:
  - name: Contract Management
    url: https://about.gitlab.com/handbook/legal/contract-management/
  - name: Corporate Law
    url: https://about.gitlab.com/handbook/legal/corporate-law/
  - name: Data Protection & Privacy
    url: https://about.gitlab.com/handbook/legal/data-protection-and-privacy/
  - name: Ethics
    url: https://about.gitlab.com/handbook/legal/ethics/
  - name: Intellectual Property
    url: https://about.gitlab.com/handbook/legal/intellectual-property/
  - name: Litigation
    url: https://about.gitlab.com/handbook/legal/litigation/
  - name: Open Source
    url: https://about.gitlab.com/handbook/legal/open-source/
  - name: Regulatory Compliance
    url: https://about.gitlab.com/handbook/legal/regulatory-compliance/
  - name: GitLab Code of Business Conduct and Ethics
    url: https://ir.gitlab.com/static-files/7d8c7eb3-cb17-4d68-a607-1b7a1fa1c95d
